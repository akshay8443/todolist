//
//  ViewController.m
//  toDoList
//
//  Created by Cli16 on 9/30/15.
//  Copyright (c) 2015 Cli16. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    
    IBOutlet UILabel *label1;
    IBOutlet UIButton *button1;
    IBOutlet UIButton *button2;
    
    NSMutableArray *data;
}
@property (strong, nonatomic) IBOutlet UITableView *tableview1;
@property (strong, nonatomic) IBOutlet UITextField *textfield1;

@end

@implementation ViewController
@synthesize tableview1;

- (void)viewDidLoad {
   
    data = [NSMutableArray new];

    _textfield1.hidden = TRUE;
    button2.hidden=TRUE;
    

    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

-(IBAction)btnPressed{
    
    _textfield1.hidden = FALSE;
    button2.hidden=FALSE;

    
    [data addObject: _textfield1.text];
    [tableview1 reloadData];
    _textfield1.text = @"";
    
}

-(IBAction)btnPressed1{
    
    _textfield1.hidden = FALSE;
    
    
  //  [data addObject: _textfield1.text];
    //[tableview1 reloadData];
    //_textfield1.text = @"";
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return data.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuse" ];
    cell.textLabel.text = data[indexPath.row];
    return cell;
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
