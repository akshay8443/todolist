//
//  AppDelegate.h
//  toDoList
//
//  Created by Cli16 on 9/30/15.
//  Copyright (c) 2015 Cli16. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

